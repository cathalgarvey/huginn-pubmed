# Huginn-Pubmed

This agent will permit you to keep abreast of research as it is added to the Pubmed academic literature index.

It operates a search using the NCBI etools, sorting by date for most recent research, and remembering what it has seen and retreived before. All that is left for you to do is to identify yourself to the etools, define the search query, the maximum record count, and the time between queries.

Your agent will then generate a regular feed of summary data for new research.


## Installation

This gem is run as part of the [Huginn](https://github.com/huginn/huginn) project. If you haven't already, follow the [Getting Started](https://github.com/huginn/huginn#getting-started) instructions there.

This is currently under development and not published to Ruby Gems.
Add this string to your Huginn's .env `ADDITIONAL_GEMS` configuration (note the inconsistent underscore and dash..):

```ruby
huginn_pubmed(git: https://gitlab.com/cathalgarvey/huginn-pubmed.git)
# when only using this agent gem it should look like this:
ADDITIONAL_GEMS=huginn_pubmed(git: https://gitlab.com/cathalgarvey/huginn-pubmed.git)
```

And then execute:

    $ bundle

## Usage

Usage instructions are provided in the Agent. The short version is that you can just provide a valid search query for Pubmed, and leave the other options at their defaults (or perhaps absent), and hopefully it will just work.

You can customise a lot, but not all, behaviour however: You can change user-agent, tool, and email, which may help NCBI attribute erroneous behaviour and notify you (you should notify them in advance according to their policies of the tool and email you're using, though). You can change the size of the "previously seen research" history, and the number of records to fetch with each query. And, you can customise timeout settings, though the default is as low as you should probably go already.

The agent emits each found article as an individual event, after sorting them by publication date and distilling the ESummaries API output into something smaller and more barebones. You'll get results that look like this:

```json
{
  "title": "Elevated Expression of Cytosolic Phospholipase A2 Delta Is Associated with Lipid Metabolism Dysregulation during Hepatocellular Carcinoma Progression.",
  "pubmed_url": "https://www.ncbi.nlm.nih.gov/pubmed/31606962",
  "uid": "31606962",
  "authors": [
    {
      "name": "Ranjpour M",
      "authtype": "Author",
      "clusterid": ""
    },
    {
      "name": "Wajid S",
      "authtype": "Author",
      "clusterid": ""
    },
    {
      "name": "Jain SK",
      "authtype": "Author",
      "clusterid": ""
    }
  ],
  "journal": "Cell journal",
  "pubdate": "2020/04/01 00:00",
  "firstauthor": "Ranjpour M",
  "lastauthor": "Jain SK",
  "article_ids": {
    "pubmed": "31606962",
    "doi": "10.22074/cellj.2020.6527",
    "rid": "31606962",
    "eid": "31606962"
  }
}
```

The NCBI 'uid' value probably makes a good key for doing deduplication downstream, if necessary, for example if merging the results of multiple Pubmed Search agents. 'pubdate' should be a reliable field for sorting, as this value is taken from a field that is intended for sorting in the NCBI output.

## Known Issues

There are a few known issues that need to be fixed:
* For now, downloader parameters like user-agent, rate-limits etcetera, are not updated if you change the settings after the first run, but remain in memory for use next time. This requires a minor refactor, perhaps it'll get done soon.
* The architecture of the agent could perhaps be broken out into a separate Gem for Pubmed queries, right now it's a little untidy.
* Probably lots of the ways I handle arguments to constructors etc. are un-idiomatic, as this is my first Ruby project.

There are also some gotchas that are outside the scope of this Agent, including:
* NCBI Pubmed performs query expansion on terms within your search query, which could potentially lead to searches that are more broad than you expected. Familiarity with Pubmed's search engine generally would be valuable to users of this agent.

## Development

Running `rake` will clone and set up Huginn in `spec/huginn` to run the specs of the Gem in Huginn as if they would be build-in Agents.
The desired Huginn repository and branch can be modified in the `Rakefile`:

```ruby
HuginnAgent.load_tasks(branch: '<your branch>', remote: 'https://github.com/<github user>/huginn.git')
```

Make sure to delete the `spec/huginn` directory and re-run `rake` after changing the `remote` to update the Huginn source code.

After the setup is done `rake spec` will only run the tests, without cloning the Huginn source again.

To install this gem onto your local machine, run `bundle exec rake install`. To release a new version, update the version number in `version.rb`, and then run `bundle exec rake release` to create a git tag for the version, push git commits and tags, and push the `.gem` file to [rubygems.org](https://rubygems.org).

## Contributing

1. Fork it ( https://github.com/[my-github-username]/huginn_pubmed/fork )
2. Create your feature branch (`git checkout -b my-new-feature`)
3. Commit your changes (`git commit -am 'Add some feature'`)
4. Push to the branch (`git push origin my-new-feature`)
5. Create a new Pull Request
