require "json"
require "faraday"


class PubmedEutils
    @@default_uagent = "Huginn Pubmed Agent (https://gitlab.com/cathalgarvey/huginn-pubmed)"
    @@default_toolname = "huginn_pubmed_agent"
    @@esearch_url = "https://eutils.ncbi.nlm.nih.gov/entrez/eutils/esearch.fcgi"
    @@esummary_url = "https://eutils.ncbi.nlm.nih.gov/entrez/eutils/esummary.fcgi"
  
    def initialize(user_agent=nil, toolname=nil, email=nil, request_delay=0.4, rate_limit_until=nil)
      @request_delay    = (request_delay ||= 0.4)
      @rate_limit_until = (rate_limit_until ||= Time.now.to_f)
      @user_agent       = (user_agent ||= @@default_uagent)
      @toolname         = (toolname ||= @@default_toolname)
      @email            = email
    end

    def self.restore(state)
      nu = PubmedEutils.new(
        user_agent=state['user_agent'],
        toolname=state['toolname'],
        email=state['email'],
        request_delay=state['request_delay'],
        rate_limit_until=state['rate_limit_until']
      )
      nu
    end

    def get_state()
      {
        'request_delay' => @request_delay,
        'rate_limit_until' => @rate_limit_until,
        'user_agent' => @user_agent,
        'toolname' => @toolname,
        'email' => @email
      }
    end
    
    def rate_limit
      # EUtils are quick to ban if you don't respect rate limits,
      # and this tool typically makes 2+ requests in rapid succession.
      sleep [0, @rate_limit_until -Time.now.to_f].max
      @rate_limit_until = Time.now.to_f + @request_delay
    end

    def esearch(query, max_results=30)
      # TODO: Think the matching MeSH keywords come from here,
      # these should be propagated forward
      params = {}
      params['term'] = query
      params['retmax'] = max_results
      params['db'] = 'pubmed'
      params['retmode'] = 'json'
      params['usehistory'] = 'n'
      params['sort'] = 'pub date'
      params['tool'] = @toolname
      if @email
        params['email'] = @email
      end
      headers = {}
      headers['User-Agent'] = @user_agent
      rate_limit
      response = Faraday.get(@@esearch_url, params, headers)
      results = JSON.load response.body
      results['esearchresult']['idlist']
    end

    def esummary(pmids)
      if pmids.empty?
        return []
      end
      params = {}
      params['db'] = 'pubmed'
      params['retmode'] = 'json'
      params['id'] = pmids.join(',')
      params['tool'] = @toolname
      if @email
        params['email'] = @email
      end
      headers = {}
      headers['User-Agent'] = @user_agent
      rate_limit
      response = Faraday.post(@@esummary_url, params, headers)
      results = JSON.load response.body
      esummaries = []
      results['result']['uids'].each {
        |uid| esummaries.push(results['result'][uid])
      }
      esummaries.sort_by! {|esummary| esummary['sortpubdate'] }
      esummaries
    end
end


module Agents
  class Pubmed < Agent
    @@default_uagent = "Huginn Pubmed Agent (https://gitlab.com/cathalgarvey/huginn_pubmed)"
    @@default_toolname = "huginn_pubmed_agent"
    @@esearch_url = "https://eutils.ncbi.nlm.nih.gov/entrez/eutils/esearch.fcgi"
    @@esummary_url = "https://eutils.ncbi.nlm.nih.gov/entrez/eutils/esummary.fcgi"
    @@ratelimit_delay = 0.4  # Seconds

    default_schedule '24h'
    cannot_receive_events!  # TODO
    
    description <<-MD
      Runs a search on Pubmed for up-to-date research, remembering seen
      literature. Use this to set up a feed of interesting research,
      including summary information like title, abstract, and authorship.

      The **pubmed_search** option indicates the desired search query. Note
      that Pubmed will perform query expansion, and some unexpected results
      may be returned if the expanded query is too generic in scope.

      The options **max_results** and **history_size** help control the
      feed volume and deduplication of incoming articles: the history is
      simply a set of previously-seen pubmed-IDs, so it does not occupy
      a lot of space. IDs are kept in a FIFO manner, so that by the time
      older IDs are pushed out of memory, the article should be unlikely
      to turn up again in the chronological search performed by default
      by this agent.

      This uses the NCBI eutils API, which requires you to identify yourself
      and respect some very reasonable rate-limits. See the EUtils usage
      guidelines here: https://www.ncbi.nlm.nih.gov/books/NBK25497/#chapter2.Usage_Guidelines_and_Requiremen
      By default this enforces a 0.4s delay between requests, which should
      be fine. You can customise this delay with the
      **request_delay_seconds** option. Do not put this below 0.4 or you
      risk getting banned.

      This agent supports **tool** and **email** options which are used
      for this purpose. This agent also uses an informative User-Agent:
      "Huginn Pubmed Agent (https://gitlab.com/cathalgarvey/huginn_pubmed)".
      A custom user-agent string may be provided as the **user_agent**
      option.

      Please use this Agent responsibly.
    MD

    event_description <<-MD
    Pubmed events look like the below example. The 'uid' field should be
    unique enough for deduplication, and the 'pubdate' field should be a
    stable format for sorting by string value to sort chronologically.
    Events are already emitted in chronological order, however, and should
    be unique already.

    {
      "title": "How did I get so late so soon? A review of time processing and management in autism.",
      "pubmed_url": "https://www.ncbi.nlm.nih.gov/pubmed/31376445",
      "uid": "31376445",
      "authors": [
        {
          "name": "Jurek L",
          "authtype": "Author",
          "clusterid": ""
        },
        ...
      ],
      "journal": "Behavioural brain research",
      "pubdate": "2019/11/18 00:00",
      "firstauthor": "Jurek L",
      "lastauthor": "Geoffray MM",
      "article_ids": {
        "pubmed": "31376445",
        "pii": "S0166-4328(19)30514-5",
        "doi": "10.1016/j.bbr.2019.112121",
        "rid": "31376445",
        "eid": "31376445"
      }
    }
    MD
    
    def default_options
      {
        "pubmed_search" => "bioremediation",
        "tool" => @@default_toolname,
        "user_agent" => @@default_uagent,
        "max_results" => "30",
        "history_size" => "300",
      }
    end

    def validate_options
      errors.add(:base, "Required argument: pubmed_search") unless interpolated['pubmed_search'].present?
    end

    def working?
      checked_without_error?
      # TODO: Implement a more effective way to check it's working.
      #received_event_without_error?
    end

    def check
      client = get_eutils_client
      memory['seen'] ||= []
      pmids = client.esearch(
        interpolated['pubmed_search'],
        max_results=interpolated.fetch('max_results', 30))
      cache_client client
      unseen = pmids.reject {|pmid| memory['seen'].include? pmid }
      summaries = client.esummary(unseen)
      cache_client client
      summaries.each { |summary|
        tidier_sum = tidy_summary(summary)
        create_event :payload => tidier_sum
      }
      memory['seen'] += unseen
      memory['seen'].uniq!
      memory['seen'] = memory['seen'].last(
        interpolated.fetch('history_size', "300").to_i)
    end

    def get_eutils_client
      # TODO: Refactor so we're only remembering the delay-until bit,
      # otherwise changes to user_agent/tool/email/request_delay
      # will not reach the cached settings for a preexisting agent.
      if !memory['client_state'].present?
        client = PubmedEutils.new(
          user_agent=interpolated['user_agent'],
          toolname=interpolated['tool'],
          email=interpolated['email'],
          request_delay=interpolated['request_delay_seconds']
        )
        memory['client_state'] = client.get_state
        return client
      end
      client = PubmedEutils.restore(memory['client_state'])
      return client
    end

    def cache_client(client)
      memory['client_state'] = client.get_state()
    end
    
    def tidy_summary(doc)
      # TODO, convert from JS:
      event = {
        "title" => doc["title"],
        "pubmed_url" => "https://www.ncbi.nlm.nih.gov/pubmed/"+doc["uid"],
        "uid" => doc["uid"],
        "authors" => doc["authors"],
        "journal" => doc["fulljournalname"],
        "pubdate" => doc["sortpubdate"],
        "firstauthor" => doc["sortfirstauthor"],
        "lastauthor" => doc["lastauthor"]
      }
      docids = {}
      doc["articleids"].each { |aid| docids[aid["idtype"]] = aid["value"] }
      event["article_ids"] = docids
      return event
    end

    #    def receive(incoming_events)
    # Future idea: searches a-la-carte using incoming queries
    #    end

  end
end
