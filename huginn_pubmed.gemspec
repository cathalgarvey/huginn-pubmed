# coding: utf-8
lib = File.expand_path('../lib', __FILE__)
$LOAD_PATH.unshift(lib) unless $LOAD_PATH.include?(lib)

Gem::Specification.new do |spec|
  spec.name          = "huginn_pubmed"
  spec.version       = '0.1'
  spec.authors       = ["Cathal Garvey"]
  spec.email         = ["cathalgarvey@cathalgarvey.me"]

  spec.summary       = %q{A Huginn agent to provide a feed of up-to-date research from Pubmed}
  spec.description   = %q{A Huginn agent to provide a feed of up-to-date research from Pubmed. This agent uses a user-defined search query and periodically issues a new query, with a built-in ability to de-duplicate previously seen records. Records are emitted in a terser, tidier format than the Pubmed EUtils default.}

  spec.homepage      = "https://gitlab.com/cathalgarvey/huginn_pubmed"


  spec.files         = Dir['LICENSE.txt', 'lib/**/*']
  spec.executables   = spec.files.grep(%r{^bin/}) { |f| File.basename(f) }
  spec.test_files    = Dir['spec/**/*.rb'].reject { |f| f[%r{^spec/huginn}] }
  spec.require_paths = ["lib"]

  spec.add_development_dependency "bundler", "~> 1.7"
  spec.add_development_dependency "rake", "~> 10.0"

  spec.add_runtime_dependency "huginn_agent"
end
